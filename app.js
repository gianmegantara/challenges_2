const express = require('express')
const port = 3031
const route = require('./app/routes/route')
const morgan = require('morgan')

let app = express()
app.use(morgan('common'))
app.use(express.json())
require('./app/routes/route')(app)



app.use(route)

app.listen(port, () => {
    console.log(`Server was running on port ${port}`)
})