const books = require('../../models/books')

exports.postBook = async (req, res) => {
    try {
        let book = new books(req.body)
        console.log(book)
        let result = await book.save()
        res.status(201).send(result)
    } catch (error) {
        res.status(500).send(error)
    }
}

exports.getBooks = async (req, res) => {
    try {
        let result = await books.find().exec()
        console.log(result)
        res.status(201).send({
            data: result,
            message: 'Books succesfully get from collection'
        })
    } catch (error) {
        res.status(201).send(error)
    }
}

exports.getBook = async (req, res) => {
    try {
        let data = await books.find().exec()
        if (data) {
            try {
                let result = await books.findById(req.params.id).exec()
                if (!result) {
                    res.status(404).send({
                        message: 'data not found'
                    })
                } else {
                    await res.status(201).send({
                        data: result,
                        message: `Book with id ${req.params.id} succesfully get`
                    })
                }
            } catch (error) {
                res.status(400).send({
                    message: 'not found'
                })
            }
        }
    } catch (error) {
        res.status(400).send({
            message: 'not found'
        })
    }


}


exports.putBook = async (req, res) => {
    try {
        let data = await books.findById(req.params.id).exec()
        console.log(data)
        if (data) {
            try {
                data.set(req.body)
                console.log("result")
                let result = await data.save()
                console.log(result)
                res.status(201).send({
                    data: data,
                    message: `Book with id ${result} succesfully updated`
                })
            } catch (error) {
                res.status(500).send(error)
            }
        } else {
            res.send({
                message: 'not found'
            })
        }
    } catch (error) {
        res.status(400).send({
            message: 'not found'
        })
    }
}


exports.deleteBook = async (req, res) => {
    try {
        const param = {
            '_id': req.params.id
        }
        let data = await books.findById(req.params.id)
        if (data) {
            try {
                let result = await books.deleteOne(param).exec()
                res.status(201).send({
                    data: result,
                    message: `Books with id ${req.params.id} was deleted`
                })
            } catch (error) {
                res.status(500).send(error)
            }
        } else {
            res.send({
                message: 'data not found'
            })
        }
    } catch (error) {
        res.send({
            message: 'data not found'
        })
    }

}