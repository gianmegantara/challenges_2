const controllerBook = require('../controllers/books')
const {
    validationRules,
    validate
} = require('../middleware/inputValidation')

module.exports = (app) => {
    app.post('/books', validationRules(), validate, controllerBook.postBook)
    app.get('/books', controllerBook.getBooks)
    app.get('/books/:id', controllerBook.getBook)
    app.put('/books/:id', controllerBook.putBook)
    app.delete('/books/:id', controllerBook.deleteBook)
    app.use((req, res, next) => {
        return res.status(404).send({
            status: 404,
            message: "not found"
        })
    })
    app.use((err, req, res, next) => {
        return res.status(500).send(err)
    })
}