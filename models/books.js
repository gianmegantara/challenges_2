const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/perpustakaan')
const books = mongoose.model('book', {
    title: {
        type: String
    },
    author: {
        type: String
    },
    published_date: {
        type: Date,
        default: Date.now
    },
    pages: {
        type: Number
    },
    language: {
        type: String
    },
    publisher_id: {
        type: String
    }
})

module.exports = books